<?php

/**
 * @file
 *
 * Provides security enhancements to core's forms.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function secure_forms_form_user_pass_alter(&$form, &$form_state) {
  // Override form validation with our own handler.
  // First remove the user module validation hook so other modules can still
  // add validation.
  foreach ($form['#validate'] as $idx => $hook) {
    if ($hook == 'user_pass_validate') {
      unset($form['#validate'][$idx]);
    }
  }
  $form['#validate'][] = 'secure_forms_user_pass_validate';

  // Add the original submit handlers to the form state for us to use later.
  $form_state['original_submit_handlers'] = $form['#submit'];
  // Remove original submit handlers.
  $form['#submit'] = array();
}

/**
 * Modified validation handler for the user pass form
 * @see user.pages.inc
 */
function secure_forms_user_pass_validate($form, &$form_state) {
  $name = trim($form_state['values']['name']);
  // Try to load by email.
  $users = user_load_multiple(array(), array('mail' => $name, 'status' => '1'));
  $account = reset($users);
  if (!$account) {
    // No success, try to load by name.
    $users = user_load_multiple(array(), array('name' => $name, 'status' => '1'));
    $account = reset($users);
  }
  if (isset($account->uid)) {
    form_set_value(array('#parents' => array('account')), $account, $form_state);

    // Call the original form submission handlers.
    $form['#submit'] = $form_state['original_submit_handlers'];
    form_execute_handlers('submit', $form, $form_state);
  }
  else {
    // On fail, we do the same as the submission handler except don't actually send the email
    // This is so that malicious users can't gain knowledge of real accounts in the system.
    drupal_set_message(t('Further instructions have been sent to your e-mail address.'));
    $form_state['redirect'] = 'user';
  }
}
